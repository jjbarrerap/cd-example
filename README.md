# Structure

#### Automatic steps
Commit > Push > Build > Test 

#### Manual Steps

* Publish as internal
* Promote internal to beta
* Promote beta to alpha
* Promote alpha to production (only in master branch)

# Requerimients

##  Necessary Files
* [Gemfile](Gemfile)
* [Gemfile.lock](Gemfile.lock)
* [Dockerfile](Dockerfile)
* [Appfile](fastlane/Appfile)
* [Fastfile](fastlane/Fastfile)
* [Util scripts](scripts/config.gradle)

## Get Access To Play Store API
1. Create a [Service Account]
    <p align="center">
        <br> <img src="./img/sa.png" alt="SA" width="500"/> <br><br>
    </p>
    <i>Assignment of permissions is optional</i>
2. Create a Key in JSON format
    <p align="center">
        <br> <img src="./img/key.png" alt="Key" width="300"/> <br><br>
    </p>
3. Access `Service Account` list
    <p align="center">
        <br> <img src="./img/sa-list.png" alt="SA list" width="200"/> <br><br>
    </p>
4. Copy the email of the new SA 
    <p align="center">
        <br> <img src="./img/sa-email.png" alt="SA email" width="300"/> <br><br>
    </p>
3. Access `Play Store console > Settings > Users & permissions` and click on <b>INVITE NEW USER</b>
4. Paste the email on the form, select the role `Release manager` and select the apps to control
    <p align="center">
        <br> <img src="./img/ps-new-user.png" alt="User form" width="450"/> <br><br>
    </p>
    <i>You can specify other permission if needed</i>

## Environment Variables
1. Access `repository > Settings > CI / CD > Variables`
2. we need to define the following variables:
    1. <b>GPSKS</b>: json generated from Service Account (copied as it is)
    2. <b>SRK</b>: Android keystore file
        * Use the following command to pass the file to hex value
        ```bash 
            xxd -p 'input_file_name'
        ```
    3. <b>SIGNING_KEYSTORE_PASSWORD</b>: android keystore password
    4. <b>SIGNING_KEY_ALIAS</b>: android keystore alias
    5. <b>SIGNING_KEY_PASSWORD</b>: android key password

<p align="center">
    <br> <img src="./img/gl-env-vars.png" alt="Environment" width="650"/> <br><br>
</p>

# Setup Project
To automate the generation of the releases we need to obtain the version code and the version name from the build itself whit the help of the util functions:
```gradle
defaultConfig {
    applicationId "com.coppel.mx.cd"
    minSdkVersion 26
    targetSdkVersion 29
    versionCode getVersionCode(version)
    // Manually bump the server version part of the string as necessary
    versionName getVersionName(version)
    testInstrumentationRunner "androidx.test.runner.AndroidJUnitRunner"
}
```


To make the generation of the singed apk  se need to set the configuration of the release in the build.gradle of the app:
``` gradle
release {
    // You need to specify either an absolute path or include the
    // keystore file in the same directory as the build.gradle file.
    storeFile file("../secrets/cd-test.keystore")
    storePassword "${secrets['SIGNING_KEYSTORE_PASSWORD']}"
    keyAlias "${secrets['SIGNING_KEY_ALIAS']}"
    keyPassword "${secrets['SIGNING_KEY_PASSWORD']}"
}
```

# Extra: Local Development
To avoid setting environment variables you can create a dir called secrets in the root of the project
<p align="center">
    <br> <img src="./img/secrets.png" alt="Environment" width="500"/> <br><br>
</p>

The directory `secrets` is already marked in the .gitignore file.

#### **`config.properties`**
``` properties
APP_VERSION_CODE=2
APP_VERSION_NAME=1.0.1
```
#### **`secrets.properties`**
``` properties
SIGNING_KEYSTORE_PASSWORD=********
SIGNING_KEY_PASSWORD=*********
SIGNING_KEY_ALIAS=cd-test
```

# TO-DO
- [x] Implement fastlane.
- [x] Automated build.
- [x] Automated test.
- [] Deploy to play store.
  - [x] Manual deploy.
  - [] Automatic deploy.
- [x] Use of semantic versioning.
- [] Automate generation of changelog.
- [] Integration with crashlytics.


# References 
[Fastlane](https://docs.fastlane.tools)

[Gilab CI + Fastlane](https://about.gitlab.com/blog/2019/01/28/android-publishing-with-gitlab-and-fastlane/)
